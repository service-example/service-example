SERVICE EXAMPLE:

- Demonstrate a service consisting of: 
  + API Gateway (API Service)
  + Authentication Service
  + Service Discovery
  + Message Broker
  + Service A (Person Service - Manage, organize and store contacts)
  + Service B (File Service - Store person attachment files and images)
- Share common libs and DTOs
- Provide singular and plural deployment descriptions

TECHNOLOGIES:
- Jakarta
- Spring, Quarkus
- MongoDB (File Data)
- Eureka (Service Discovery)
- Keycloak (Authentication)
- Kafka (Messaging)